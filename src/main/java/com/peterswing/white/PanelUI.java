package com.peterswing.white;

import java.awt.Graphics;

import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicPanelUI;

public class PanelUI extends BasicPanelUI {
	private static PanelUI panelUI;

	public static ComponentUI createUI(JComponent c) {
		if (panelUI == null) {
			panelUI = new PanelUI();
		}
		return panelUI;
	}

	public void installUI(JComponent c) {
		super.installUI(c);
	}

	public void uninstallUI(JComponent c) {
		super.uninstallUI(c);
	}

//	public void update(Graphics g, JComponent c) {
//
//	}
//
//	public void paint(Graphics g, JComponent c) {
//	}

	//	public void paint(Graphics g, JComponent c) {
	//		boolean disable = false;
	//		if (c.getClientProperty("disable") != null) {
	//			disable = (boolean) c.getClientProperty("disable");
	//		}
	//		if (!disable) {
	//			for (Component com : c.getComponents()) {
	//				com.setVisible(true);
	//			}
	//			super.paint(g, c);
	//		} else {
	//			for (Component com : c.getComponents()) {
	//				com.setVisible(false);
	//			}
	//			//			c.setOpaque(true);
	//			//			c.setBackground(Color.black);
	//			g.setColor(new Color(0, 0, 0, 0.5f));
	//			g.fillRect(0, 0, c.getWidth(), c.getHeight());
	//		}
	//	}
}
