package com.peterswing.white;

import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.Icon;
import javax.swing.tree.TreeNode;

import com.peterswing.CommonLib;

public class FileChooserTreeNode implements TreeNode {
	private Icon icon;
	File file;
	private String text;
	//	boolean isDirectory;
	boolean noIcon;
	private Vector<FileChooserTreeNode> childNodes = null;
	FileChooserTreeNode parent;

	//	public FileChooserTreeNode(String text, File file) {
	//		this.text = text;
	//		this.file = file;
	//		//		this.isDirectory = isDirectory;
	//	}

	//	public FileChooserTreeNode(String text, File file, boolean noIcon) {
	//		this(text, file);
	//		this.noIcon = noIcon;
	//	}

	public FileChooserTreeNode(String text, File file, boolean noIcon, FileChooserTreeNode parent) {
		this.text = text;
		this.file = file;
		this.noIcon = noIcon;
		this.parent = parent;
	}

	void init() {
		childNodes = new Vector<FileChooserTreeNode>();
		if (file != null && file.isDirectory()) {
			File files[] = file.listFiles(new DirectoryFilter());
			if (files == null) {
				return;
			}
			Arrays.sort(files, new Comparator() {
				public int compare(final Object o1, final Object o2) {
					return ((File) o1).getName().toLowerCase().compareTo(((File) o2).getName().toLowerCase());
				}
			});
			for (int x = 0; x < files.length; x++) {
				childNodes.add(new FileChooserTreeNode(null, files[x], files[x].isDirectory(), this));
			}
		}
	}

	public Icon getIcon() {
		return icon;
	}

	public void setIcon(Icon icon) {
		this.icon = icon;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	@Override
	public TreeNode getChildAt(int childIndex) {
		//		if (file != null && file.isDirectory() && file.listFiles(new DirectoryFilter()) != null) {
		//			File files[] = file.listFiles(new DirectoryFilter());
		//			Arrays.sort(files, new Comparator() {
		//				public int compare(final Object o1, final Object o2) {
		//					return ((File) o1).getName().toLowerCase().compareTo(((File) o2).getName().toLowerCase());
		//				}
		//			});
		//
		//			File child = files[childIndex];
		//			return new FileChooserTreeNode(child.getName(), child, true, this);
		//		} else {
		if (childNodes == null) {
			init();
		}
		return childNodes.get(childIndex);
		//		}
	}

	@Override
	public int getChildCount() {
		//		if (file != null && file.isDirectory() && file.listFiles(new DirectoryFilter()) != null) {
		//			return file.listFiles(new DirectoryFilter()).length;
		//		} else {
		if (childNodes == null) {
			init();
		}
		return childNodes.size();
		//		}
	}

	@Override
	public TreeNode getParent() {
		return parent;
	}

	@Override
	public int getIndex(TreeNode node) {
		//		FileChooserTreeNode n = (FileChooserTreeNode) node;
		//		File files[] = file.listFiles(new DirectoryFilter());
		//		for (int x = 0; x < files.length; x++) {
		//			if (files[x].getName().equals(n.file.getName())) {
		//				return x;
		//			}
		//		}

		if (childNodes == null) {
			init();
		}
		for (int x = 0; x < childNodes.size(); x++) {
			if (childNodes.get(x).file.getName().equals(((FileChooserTreeNode) node).file.getName())) {
				return x;
			}
		}
		return -1;
	}

	@Override
	public boolean getAllowsChildren() {
		return true;
	}

	@Override
	public boolean isLeaf() {
		//		if (file != null && file.isDirectory() && file.listFiles(new DirectoryFilter()) != null) {
		//			return file.listFiles(new DirectoryFilter()).length == 0;
		//		} else {
		if (childNodes == null) {
			init();
		}
		return childNodes.size() == 0;
		//		}
	}

	@Override
	public Enumeration children() {
		// return new Vector(Arrays.asList(file.listFiles(new DirectoryFilter()))).elements();
		//		if (file == null) {
		return CommonLib.makeEnumeration(childNodes.toArray());
		//		} else {
		//			File files[] = file.listFiles(new DirectoryFilter());
		//			FileChooserTreeNode a[] = new FileChooserTreeNode[files.length];
		//
		//			for (int x = 0; x < a.length; x++) {
		//				a[x] = new FileChooserTreeNode(null, files[x], files[x].isDirectory(), this);
		//			}
		//			return CommonLib.makeEnumeration(a);
		//		}
	}

	public void add(FileChooserTreeNode node) {
		if (childNodes == null) {
			init();
		}
		childNodes.add(node);
	}

	public String toString() {
		return (text == null) ? file.getName() : text;
	}

	class DirectoryFilter implements FileFilter {
		public boolean accept(File file) {
			return file.isDirectory();
		}
	}
}
