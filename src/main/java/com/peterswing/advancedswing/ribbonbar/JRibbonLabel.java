package com.peterswing.advancedswing.ribbonbar;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class JRibbonLabel extends JLabel {
	Color gray = new Color(105, 105, 105);

	public JRibbonLabel() {
		super();
		initGUI();
	}

	public JRibbonLabel(String text) {
		super(text);
		initGUI();
	}

	void initGUI() {
		setHorizontalTextPosition(SwingConstants.CENTER);
		setHorizontalAlignment(SwingConstants.CENTER);
		setForeground(gray);
		setFont(new Font("Dialog", Font.PLAIN, 10));
	}

	public void paint(Graphics g) {
		FontMetrics fm = getFontMetrics(getFont());
		int fontWidth = fm.stringWidth(getText()) + 10;
		int offsetY = 8;

		g.setColor(gray);
		g.drawLine(0, getHeight() - offsetY, (getWidth() - fontWidth) / 2, getHeight() - offsetY);
		g.drawLine((getWidth() + fontWidth) / 2, getHeight() - offsetY, getWidth(), getHeight() - offsetY);
		super.paint(g);
	}
}
