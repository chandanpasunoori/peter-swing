package com.peterswing.advancedswing.enhancedtextarea;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.StringWriter;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.WindowConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter.DefaultHighlightPainter;
import javax.swing.text.Element;
import javax.swing.text.Highlighter;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.peterswing.CommonLib;
import com.peterswing.advancedswing.pager.Pager;
import com.peterswing.advancedswing.searchtextfield.JSearchTextField;

public class EnhancedTextArea extends JPanel implements DocumentListener {
	public JToolBar toolBar;
	private JScrollPane scrollPane1;
	public Pager pager;
	private JButton clearButton;
	private JComboBox fontComboBox;
	private JLabel searchLabel;
	private JPanel statusPanel;
	private JSearchTextField searchTextField;
	private JLabel statusLabel;
	private JButton fontBiggerButton;
	private JButton fontSmallerButton;
	private JToggleButton lineWrapButton;
	public JTextArea textArea;
	private JTextArea lines;
	private int maxRow = -1;
	private String str;
	private JLabel separatorLabel;
	private JLabel label1;
	public boolean separateByLine = true;
	public int pageSize = 20;
	public int lineNoBase = 0;
	public JButton saveButton;

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.getContentPane().add(new JTextArea());
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}

	public EnhancedTextArea() {
		super();

		try {
			BorderLayout thisLayout = new BorderLayout();
			this.setLayout(thisLayout);
			this.setPreferredSize(new java.awt.Dimension(725, 290));

			toolBar = new JToolBar();
			this.add(toolBar, BorderLayout.NORTH);

			saveButton = new JButton();
			toolBar.add(saveButton);
			saveButton.setText("Save");
			saveButton.setIcon(new ImageIcon(getClass().getClassLoader().getResource("com/peterswing/advancedswing/enhancedtextarea/disk.png")));
			saveButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					jSaveButtonActionPerformed(evt);
				}
			});

			clearButton = new JButton();
			toolBar.add(clearButton);
			clearButton.setText("Clear");
			clearButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					clearButtonActionPerformed(evt);
				}
			});

			lineWrapButton = new JToggleButton();
			toolBar.add(lineWrapButton);
			lineWrapButton.setText("Wrap");
			lineWrapButton.setIcon(new ImageIcon(getClass().getClassLoader().getResource("com/peterswing/advancedswing/enhancedtextarea/linewrap.png")));
			lineWrapButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					jLineWrapButtonActionPerformed(evt);
				}
			});

			fontBiggerButton = new JButton();
			toolBar.add(fontBiggerButton);
			fontBiggerButton.setIcon(new ImageIcon(getClass().getClassLoader().getResource("com/peterswing/advancedswing/enhancedtextarea/font_add.png")));
			fontBiggerButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					jFontBiggerButtonActionPerformed(evt);
				}
			});

			fontSmallerButton = new JButton();
			toolBar.add(fontSmallerButton);
			fontSmallerButton.setIcon(new ImageIcon(getClass().getClassLoader().getResource("com/peterswing/advancedswing/enhancedtextarea/font_delete.png")));
			fontSmallerButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					jFontSmallerButtonActionPerformed(evt);
				}
			});

			label1 = new JLabel();
			toolBar.add(label1);
			label1.setText(" ");

			searchTextField = new JSearchTextField();
			toolBar.add(searchTextField);
			searchTextField.setMaximumSize(new java.awt.Dimension(100, 22));
			searchTextField.setPreferredSize(new java.awt.Dimension(100, 22));
			searchTextField.setSize(new java.awt.Dimension(100, 22));
			searchTextField.addKeyListener(new KeyAdapter() {
				public void keyReleased(KeyEvent evt) {
					jSearchTextFieldKeyReleased(evt);
				}
			});

			separatorLabel = new JLabel();
			toolBar.add(separatorLabel);
			separatorLabel.setText(" ");

			GraphicsEnvironment e = GraphicsEnvironment.getLocalGraphicsEnvironment();
			Font[] fonts = e.getAllFonts();
			String fontNames[] = new String[fonts.length];
			int x = 0;
			for (Font f : fonts) {
				fontNames[x++] = f.getFontName();
			}
			ComboBoxModel fontComboBoxModel = new DefaultComboBoxModel(fontNames);
			fontComboBox = new JComboBox();
			toolBar.add(fontComboBox);
			fontComboBox.setModel(fontComboBoxModel);
			fontComboBox.setMaximumSize(new java.awt.Dimension(180, 22));
			fontComboBox.setPreferredSize(new java.awt.Dimension(180, 22));
			fontComboBox.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evt) {
					jFontComboBoxActionPerformed(evt);
				}
			});

			pager = new Pager();
			toolBar.add(pager);
			pager.setVisible(false);

			scrollPane1 = new JScrollPane();
			this.add(scrollPane1, BorderLayout.CENTER);

			textArea = new JTextArea();
			textArea.getDocument().addDocumentListener(this);
			lines = new JTextArea(" 1 ");
			lines.setBackground(new Color(200, 230, 245));
			lines.setEditable(false);
			scrollPane1.setRowHeaderView(lines);

			textArea.getDocument().addDocumentListener(new DocumentListener() {
				public String getText() {
					int caretPosition = textArea.getDocument().getLength();
					Element root = textArea.getDocument().getDefaultRootElement();

					int base = 0;
					if (separateByLine == false) {
						if (str != null) {
							base = StringUtils.countMatches(str.substring(0, (pager.getPage() - 1) * pageSize), System.getProperty("line.separator"));
							if (base == 1) {
								base = 0;
							}
						}
					} else {
						base = (pager.getPage() - 1) * pageSize;
					}
					base += lineNoBase;
					String text = " " + (base + 1) + " " + System.getProperty("line.separator");
					for (int i = 2; i < root.getElementIndex(caretPosition) + 2; i++) {
						text += " " + (base + i) + " " + System.getProperty("line.separator");
					}
					return text;
				}

				@Override
				public void changedUpdate(DocumentEvent de) {
					lines.setText(getText());
				}

				@Override
				public void insertUpdate(DocumentEvent de) {
					lines.setText(getText());
				}

				@Override
				public void removeUpdate(DocumentEvent de) {
					lines.setText(getText());
				}

			});
			scrollPane1.setViewportView(textArea);

			statusPanel = new JPanel();
			FlowLayout jStatusPanelLayout = new FlowLayout();
			jStatusPanelLayout.setAlignment(FlowLayout.LEFT);
			statusPanel.setLayout(jStatusPanelLayout);
			this.add(statusPanel, BorderLayout.SOUTH);

			statusLabel = new JLabel();
			statusPanel.add(statusLabel);

			searchLabel = new JLabel();
			statusPanel.add(searchLabel);

			this.fontComboBox.setSelectedItem(textArea.getFont().getFamily() + ".plain");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public JTextArea getTextArea() {
		return textArea;
	}

	public void setText(String text) {
		textArea.setText(text);
	}

	public String getText() {
		return textArea.getText();
	}

	private void updateStatus() {
		statusLabel.setText("Line:" + textArea.getText().split("\n").length + ", Char:" + textArea.getText().length());
	}

	private void jLineWrapButtonActionPerformed(ActionEvent evt) {
		textArea.setLineWrap(lineWrapButton.isSelected());
	}

	private void jFontSmallerButtonActionPerformed(ActionEvent evt) {
		Font f = textArea.getFont();
		Font newFont = new Font(f.getFontName(), f.getStyle(), f.getSize() - 1);
		textArea.setFont(newFont);
		lines.setFont(newFont);
	}

	private void jFontBiggerButtonActionPerformed(ActionEvent evt) {
		Font f = textArea.getFont();
		Font newFont = new Font(f.getFontName(), f.getStyle(), f.getSize() + 1);
		textArea.setFont(newFont);
		lines.setFont(newFont);
	}

	public void addTrailListener(File directory, File file) {
		addTrailListener(directory, file, 1000, false);
	}

	public void addTrailListener(final File directory, final File file, long sampleInterval, boolean startAtBeginning) {
		if (startAtBeginning) {
			StringWriter stringWriter = new StringWriter();
			try {
				IOUtils.copy(new FileInputStream(file), stringWriter);
			} catch (Exception e) {
			}
			textArea.setText(stringWriter.toString());
		}
		new Thread("EnhancedTextArea file watcher thread") {
			public void run() {
				try {
					WatchService watchService = FileSystems.getDefault().newWatchService();
					Path path = Paths.get(directory.getAbsolutePath());
					path.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);
					long fileSize = file.length();
					while (true) {
						WatchKey key = watchService.take(); // retrieve the watchkey
						for (WatchEvent event : key.pollEvents()) {
							if (event.context().equals(file.getName())) {
								System.out.println(event.kind() + ": " + event.context()); // Display event and file name
								StringWriter stringWriter = new StringWriter();
								try {
									IOUtils.copy(new FileInputStream(file), stringWriter);
								} catch (Exception e) {
									e.printStackTrace();
								}
								newLogFileLine(stringWriter.toString().substring((int) fileSize));
								fileSize = file.length();
							}
						}
						boolean valid = key.reset();
						if (!valid) {
							break; // Exit if directory is deleted
						}
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}.start();
	}

	public void newLogFileLine(String line) {
		if (maxRow == -1) {
			textArea.append(line + System.getProperty("line.separator"));
		} else {
			if (line.split(System.getProperty("line.separator")).length > maxRow) {
				textArea.setText(line);
				try {
					textArea.replaceRange("", textArea.getLineStartOffset(0), textArea.getLineEndOffset(textArea.getLineCount() - maxRow - 1));
				} catch (BadLocationException e) {
				}
			} else {
				textArea.append(line + System.getProperty("line.separator"));
				if (textArea.getLineCount() > maxRow) {
					lineNoBase += line.split(System.getProperty("line.separator")).length;
					try {
						textArea.replaceRange("", textArea.getLineStartOffset(0), textArea.getLineEndOffset(textArea.getLineCount() - maxRow - 1));
					} catch (BadLocationException e) {
					}
				}
			}
		}
		textArea.setCaretPosition(textArea.getDocument().getLength());
		updateStatus();
	}

	private void jSearchTextFieldKeyReleased(KeyEvent evt) {
		String text = textArea.getText().toLowerCase();
		String searchPattern = searchTextField.getText().toLowerCase();

		if (evt != null && evt.getKeyCode() == 10) {
			int caretPosition = textArea.getCaretPosition();
			boolean found = false;
			for (int j = caretPosition + 1; j < text.length() - searchPattern.length(); j += 1) {
				if (searchPattern.equals(text.substring(j, j + searchPattern.length()))) {
					textArea.setCaretPosition(j);
					found = true;
					break;
				}
			}
			if (!found) {
				for (int j = 0; j < caretPosition; j++) {
					if (searchPattern.equals(text.substring(j, j + searchPattern.length()))) {
						textArea.setCaretPosition(j);
						break;
					}
				}
			}
		}

		if (searchPattern.length() > 0) {
			Highlighter h = textArea.getHighlighter();
			DefaultHighlightPainter painter = new DefaultHighlightPainter(Color.YELLOW);
			DefaultHighlightPainter painter2 = new DefaultHighlightPainter(Color.RED);
			h.removeAllHighlights();

			int count = 0;
			boolean isCurrent = false;
			for (int j = 0; j < text.length(); j += 1) {
				if (j < text.length() - searchPattern.length() && searchPattern.equals(text.substring(j, j + searchPattern.length()))) {
					count++;
					try {
						if (j >= textArea.getCaretPosition() && isCurrent == false) {
							h.addHighlight(j, j + searchPattern.length(), painter2);
							isCurrent = true;
						} else {
							h.addHighlight(j, j + searchPattern.length(), painter);
						}
					} catch (BadLocationException ble) {
					}
				}
			}
			searchLabel.setText("Match:" + count);
		} else {
			searchLabel.setText("");
			Highlighter h = textArea.getHighlighter();
			h.removeAllHighlights();
		}
	}

	@Override
	public void insertUpdate(DocumentEvent e) {
		updateStatus();
	}

	@Override
	public void removeUpdate(DocumentEvent e) {
		updateStatus();
	}

	@Override
	public void changedUpdate(DocumentEvent e) {
		updateStatus();
	}

	public int getMaxRow() {
		return maxRow;
	}

	public void setMaxRow(int maxRow) {
		this.maxRow = maxRow;
	}

	private void jFontComboBoxActionPerformed(ActionEvent evt) {
		textArea.setFont(new Font(fontComboBox.getSelectedItem().toString(), textArea.getFont().getStyle(), textArea.getFont().getSize()));
	}

	public void loadLargeFile(String str) {
		this.str = str;
		if (separateByLine == false) {
			pager.maxPageNo = str.length() / pageSize + 1;
		} else {
			pager.maxPageNo = str.split(System.getProperty("line.separator")).length / pageSize + 1;
		}
	}

	public void refreshPage() {
		if (separateByLine == false) {
			if (pager.getPage() * pageSize < str.length()) {
				this.textArea.setText(str.substring((pager.getPage() - 1) * pageSize, pager.getPage() * pageSize));
			} else {
				this.textArea.setText(str.substring((pager.getPage() - 1) * pageSize));
			}
		} else {
			String s[] = str.split(System.getProperty("line.separator"));
			int maxIndex = pager.getPage() * pageSize;
			if (maxIndex > s.length) {
				maxIndex = s.length;
			}
			//			String ss = StringUtils.join(s, System.getProperty("line.separator"), (pager.getPage() - 1) * pageSize, maxIndex);
			String ss = StringUtils.join(s, System.getProperty("line.separator"));
			this.textArea.setText(ss);
		}
	}

	public void setPage(int pageNo) {
		pager.setPageNo(pageNo);
		refreshPage();
	}

	private void jSaveButtonActionPerformed(ActionEvent evt) {
		JFileChooser fc = new JFileChooser();
		int returnVal = fc.showSaveDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			CommonLib.saveFile(this.textArea.getText(), file);
		}
	}

	private void clearButtonActionPerformed(ActionEvent evt) {
		textArea.setText("");
	}
	
	public void hideFontComboBox(){
		fontComboBox.setVisible(false);
	}
}
